const { describe, it } = require('mocha')
const assert = require('assert')
const dataGenerator = require('../src/dataGenerator')
const constants = require('../src/constants')
const _ = require('lodash')

describe('dataGenerator test suite:', function () {
  it('generate satallite ids with no number param', function () {
    const ids = dataGenerator.generateSatelliteIds()
    assert.notEqual(ids, null)
    assert.equal(ids.length, 0)
  })
  it('generate random number of satallite ids', function () {
    const num = _.random(1, 5)
    const ids = dataGenerator.generateSatelliteIds(num)
    assert.notEqual(ids, null)
    assert.equal(ids.length, num)
  })
  it('get random satallite id with null param', function () {
    const id = dataGenerator.getRandomSatelliteId(null)
    assert.equal(id, null)
  })
  it('get random satallite id', function () {
    const ids = [1, 2, 3, 4, 5]
    const id = dataGenerator.getRandomSatelliteId(ids)
    assert.notEqual(id, null)
    assert.ok(_.includes(ids, id))
  })
  it('generate raw value with no param', function () {
    const val = dataGenerator.generateRawValue(null)
    assert.equal(val, null)
  })
  it('generate raw value with unknown componentType', function () {
    const val = dataGenerator.generateRawValue('unknown')
    assert.equal(val, null)
  })
  it('generate raw value with BATT componentType', function () {
    const val = dataGenerator.generateRawValue(constants.BATTERY)
    assert.notEqual(val, null)
    assert.ok((val <= constants.BATTERY_RED_HIGH + constants.BATTERY_RANGE) &&
      (val >= constants.BATTERY_RED_LOW - constants.BATTERY_RANGE))
  })
  it('generate raw value with TSTAT componentType', function () {
    const val = dataGenerator.generateRawValue(constants.TEMP)
    assert.notEqual(val, null)
    assert.ok((val <= constants.TEMP_RED_HIGH + constants.TEMP_RANGE) &&
      (val >= constants.TEMP_RED_LOW - constants.TEMP_RANGE))
  })
  it('generateTelemetry', function () {
    const ids = [1, 2, 3, 4, 5]
    const telemetry = dataGenerator.generateTelemetry(ids)
    assert.notEqual(telemetry, null)
    assert.equal(telemetry.length, 8)
    const componentCheck = _.includes(telemetry, constants.BATTERY) ||
        _.includes(telemetry, constants.TEMP)
    assert.ok(componentCheck)
  })
})
