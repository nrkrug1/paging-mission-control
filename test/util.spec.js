const { describe, it } = require('mocha')
const assert = require('assert')
const util = require('../src/util')

describe('util test suite:', function () {
  it('Parse Date from formatted string', function () {
    const dateString = '20120101 02:03:19.113'
    const parsedDate = util.parseDate(dateString)
    assert.notEqual(parsedDate, null)
    assert.equal(parsedDate.getFullYear(), 2012)
    assert.equal(parsedDate.getMonth(), 0) // Date weirdness
    assert.equal(parsedDate.getDate(), 1)
    assert.equal(parsedDate.getHours(), 2)
    assert.equal(parsedDate.getMinutes(), 3)
    assert.equal(parsedDate.getSeconds(), 19)
    assert.equal(parsedDate.getMilliseconds(), 113)
  })
  it('Parse Date from 24hr formatted time', function () {
    const dateString = '20080103 23:53:22.472'
    const parsedDate = util.parseDate(dateString)
    assert.notEqual(parsedDate, null)
    assert.equal(parsedDate.getFullYear(), 2008)
    assert.equal(parsedDate.getMonth(), 0) // Date weirdness
    assert.equal(parsedDate.getDate(), 3)
    assert.equal(parsedDate.getHours(), 23)
    assert.equal(parsedDate.getMinutes(), 53)
    assert.equal(parsedDate.getSeconds(), 22)
    assert.equal(parsedDate.getMilliseconds(), 472)
  })
})
