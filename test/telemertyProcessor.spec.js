const { describe, it, beforeEach } = require('mocha')
const assert = require('assert')
const _ = require('lodash')

const constants = require('../src/constants')
const { TelemetryProcessor } = require('../src/telemetryProcessor')

// TelemetryProcessor
let processor

// Example Test events
const batteryEvent = [
  '20090101 00:01:13.702',
  '1002',
  '17',
  '15',
  '9',
  '8',
  '12.2',
  'BATT'
]

const tempEvent = [
  '20090101 00:01:15.147',
  '1003',
  '101',
  '98',
  '25',
  '20',
  '86',
  'TSTAT'
]

describe('TelemetryProcessor test suite:', function () {
  beforeEach(function () {
    processor = new TelemetryProcessor()
  })
  it('Handles unsupported component type', function () {
    const event = [...batteryEvent]
    event[7] = 'SONAR'

    const ret = processor._eventValueOutsideRange(event)
    assert.equal(ret, false)
  })
  it('Detects battery values within range', function () {
    // event value within range
    const ret = processor._eventValueOutsideRange(batteryEvent)
    assert.equal(ret, false)
  })
  it('Detects temp values within range', function () {
    // event value within range
    const ret = processor._eventValueOutsideRange(tempEvent)
    assert.equal(ret, false)
  })
  it('Detects battery values outside range', function () {
    const event = [...batteryEvent]
    event[6] = 5.5

    const ret = processor._eventValueOutsideRange(event)
    assert.equal(ret, true)
  })
  it('Detects temp values outside range', function () {
    const event = [...tempEvent]
    event[6] = 107

    const ret = processor._eventValueOutsideRange(event)
    assert.equal(ret, true)
  })
  it('Record battery event updates eventTracker', function () {
    processor._recordEvent(batteryEvent)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 1)

    const batteryTracker = processor._eventTracker[constants.BATTERY]
    assert.notEqual(batteryTracker, null)
    assert.equal(_.size(batteryTracker), 1)

    const satelliteBattTracker = batteryTracker[batteryEvent[1]]
    assert.equal(_.size(satelliteBattTracker), 1)
  })
  it('Record temp event updates eventTracker', function () {
    processor._recordEvent(tempEvent)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 1)

    const tempTracker = processor._eventTracker[constants.TEMP]
    assert.notEqual(tempTracker, null)
    assert.equal(_.size(tempTracker), 1)

    const satelliteTempTracker = tempTracker[tempEvent[1]]
    assert.equal(_.size(satelliteTempTracker), 1)
  })
  it('EventTracker maintains size of 3 for battery', function () {
    processor._recordEvent(batteryEvent)
    processor._recordEvent(batteryEvent)
    processor._recordEvent(batteryEvent)
    processor._recordEvent(batteryEvent)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 1)

    const batteryTracker = processor._eventTracker[constants.BATTERY]
    assert.notEqual(batteryTracker, null)
    assert.equal(_.size(batteryTracker), 1)

    const satelliteBattTracker = batteryTracker[batteryEvent[1]]
    assert.equal(_.size(satelliteBattTracker), 3)
  })
  it('EventTracker maintains size of 3 for temp', function () {
    processor._recordEvent(tempEvent)
    processor._recordEvent(tempEvent)
    processor._recordEvent(tempEvent)
    processor._recordEvent(tempEvent)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 1)

    const tempTracker = processor._eventTracker[constants.TEMP]
    assert.notEqual(tempTracker, null)
    assert.equal(_.size(tempTracker), 1)

    const satelliteTempTracker = tempTracker[tempEvent[1]]
    assert.equal(_.size(satelliteTempTracker), 3)
  })
  it('Alert returns quiety with no temp events in tracker', function () {
    processor._alert(tempEvent)

    assert.equal(_.size(processor._alerts), 0)
  })
  it('Alert returns quiety with < 3 events in tracker', function () {
    // setup
    const timestamp1 = Date.now().valueOf()
    const timestamp2 = timestamp1 + _.random(1000, 10000)
    processor._eventTracker[constants.TEMP] = {}
    processor._eventTracker[constants.TEMP][tempEvent[1]] =
      [timestamp1, timestamp2]

    // check test condition is as expected
    const tempTracker = processor._eventTracker[constants.TEMP]
    assert.notEqual(tempTracker, null)
    assert.equal(_.size(tempTracker), 1)

    const satelliteTempTracker = tempTracker[tempEvent[1]]
    assert.equal(_.size(satelliteTempTracker), 2)

    processor._alert(tempEvent)

    assert.equal(_.size(processor._alerts), 0)
  })
  it('Alert handles 3 events not in alert interval', function () {
    // should be 2x the alert interval
    const timestamp1 = Date.now().valueOf()
    const timestamp2 = timestamp1 + constants.ALERT_INTERVAL_MS
    const timestamp3 = timestamp2 + constants.ALERT_INTERVAL_MS

    processor._eventTracker[constants.TEMP] = {}
    processor._eventTracker[constants.TEMP][tempEvent[1]] =
      [timestamp1, timestamp2, timestamp3]

    // check test condition is as expected
    const tempTracker = processor._eventTracker[constants.TEMP]
    assert.notEqual(tempTracker, null)
    assert.equal(_.size(tempTracker), 1)

    const satelliteTempTracker = tempTracker[tempEvent[1]]
    assert.equal(_.size(satelliteTempTracker), 3)

    processor._alert(tempEvent)

    assert.equal(_.size(processor._alerts), 0)
  })
  it('Alert handles 3 temp events in alert interval', function () {
    // < constants.ALERT_INTERVAL_MS
    const timestamp1 = Date.now().valueOf()
    const timestamp2 = timestamp1 + 1000
    const timestamp3 = timestamp2 + 1000

    // expected values
    const alertTimestamp = new Date(timestamp1)
    const isoAlertDate =
      new Date(alertTimestamp.getTime() - alertTimestamp.getTimezoneOffset() * 60000)

    processor._eventTracker[constants.TEMP] = {}
    processor._eventTracker[constants.TEMP][tempEvent[1]] =
      [timestamp1, timestamp2, timestamp3]

    // check test condition is as expected
    const tempTracker = processor._eventTracker[constants.TEMP]
    assert.notEqual(tempTracker, null)
    assert.equal(_.size(tempTracker), 1)

    const satelliteTempTracker = tempTracker[tempEvent[1]]
    assert.equal(_.size(satelliteTempTracker), 3)

    const alertCreated = processor._alert(tempEvent)

    assert.equal(alertCreated, true)
    assert.equal(_.size(processor._alerts), 1)
    const alert = processor._alerts[0]
    assert.equal(alert.satelliteId, tempEvent[1])
    assert.equal(alert.severity, 'RED HIGH')
    assert.equal(alert.component, tempEvent[7])
    assert.equal(alert.timestamp, isoAlertDate.toISOString())
  })
  it('Alert handles 3 battery events in alert interval', function () {
    // < constants.ALERT_INTERVAL_MS
    const timestamp1 = Date.now().valueOf()
    const timestamp2 = timestamp1 + 1000
    const timestamp3 = timestamp2 + 1000

    // expected values
    const alertTimestamp = new Date(timestamp1)
    const isoAlertDate =
      new Date(alertTimestamp.getTime() - alertTimestamp.getTimezoneOffset() * 60000)

    processor._eventTracker[constants.BATTERY] = {}
    processor._eventTracker[constants.BATTERY][batteryEvent[1]] =
      [timestamp1, timestamp2, timestamp3]

    // check test condition is as expected
    const batteryTracker = processor._eventTracker[constants.BATTERY]
    assert.notEqual(batteryTracker, null)
    assert.equal(_.size(batteryTracker), 1)

    const satelliteBatteryTracker = batteryTracker[batteryEvent[1]]
    assert.equal(_.size(satelliteBatteryTracker), 3)

    const alertCreated = processor._alert(batteryEvent)

    assert.equal(alertCreated, true)
    assert.equal(_.size(processor._alerts), 1)
    const alert = processor._alerts[0]
    assert.equal(alert.satelliteId, batteryEvent[1])
    assert.equal(alert.severity, 'RED LOW')
    assert.equal(alert.component, batteryEvent[7])
    assert.equal(alert.timestamp, isoAlertDate.toISOString())
  })
  it('Process Event handles null event', function () {
    processor.processEvent(null)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 0)
    assert.equal(_.size(processor._alerts), 0)
  })
  it('Process Event handles normal battery event', function () {
    // value within range expect no change to tracker or alerts
    processor.processEvent(batteryEvent)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 0)
    assert.equal(_.size(processor._alerts), 0)
  })
  it('Process Event handles normal temp event', function () {
    // value within range expect no change to tracker or alerts
    processor.processEvent(tempEvent)
    const eventSize = _.size(processor._eventTracker)

    assert.equal(eventSize, 0)
    assert.equal(_.size(processor._alerts), 0)
  })
  it('Process Event handles alertable battery event', function () {
    const event = [...batteryEvent]
    event[6] = 5.5

    processor.processEvent(event)

    const eventSize = _.size(processor._eventTracker[constants.BATTERY])
    assert.equal(eventSize, 1)
    assert.equal(_.size(processor._alerts), 0)
  })
  it('Process Event handles alertable temp event', function () {
    const event = [...tempEvent]
    event[6] = 107
    processor.processEvent(event)

    const eventSize = _.size(processor._eventTracker)
    assert.equal(eventSize, 1)
    assert.equal(_.size(processor._alerts), 0)
  })

  // Simulate... data from sample input file
  it('Integration: Process creates alerts', function () {
    const input = [
      _.split('20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT', '|'),
      _.split('20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT', '|'),
      _.split('20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT', '|'),
      _.split('20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT', '|'),
      _.split('20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT', '|'),
      _.split('20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT', '|'),
      _.split('20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT', '|'),
      _.split('20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT', '|'),
      _.split('20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT', '|'),
      _.split('20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT', '|'),
      _.split('20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT', '|'),
      _.split('20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT', '|'),
      _.split('20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT', '|'),
      _.split('20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT', '|')
    ]

    _.each(input, function (event) {
      processor.processEvent(event)
    })

    const batteryEventSize = _.size(processor._eventTracker[constants.BATTERY])
    assert.equal(batteryEventSize, 2)
    const tempEventSize = _.size(processor._eventTracker[constants.TEMP])
    assert.equal(tempEventSize, 1)

    assert.equal(_.size(processor._alerts), 2)

    // Compare against sample output - copied from README.md
    const tempAlert = processor._alerts[0]
    assert.equal(tempAlert.satelliteId, '1000')
    assert.equal(tempAlert.severity, 'RED HIGH')
    assert.equal(tempAlert.component, 'TSTAT')
    assert.equal(tempAlert.timestamp, '2018-01-01T23:01:38.001Z')

    const batteryAlert = processor._alerts[1]
    assert.equal(batteryAlert.satelliteId, '1000')
    assert.equal(batteryAlert.severity, 'RED LOW')
    assert.equal(batteryAlert.component, 'BATT')
    assert.equal(batteryAlert.timestamp, '2018-01-01T23:01:09.521Z')
  })
})
