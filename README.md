# Solution Description
There are two modules included in this solution: one to process input files and generate alerts and a supporting module to generate test data. Both modules utilitze file streaming libraries (fs) provided by the Node ecosystem. Streams were chosen to allow the program to handle any size file without the need utilize large amounts of memory. 

**Language**: JavaScript  
**Runtime**: node  
**Task Runner**: npm  
**Unit Tests**: mocha, assert  
**Linter**: standard  

# Usage

## Install Dependencies
**npm install**

## Run all tests
**npm run test**  Note: also runs linter

## Run program
**npm run main** or with optional fileName: **npm run main -- --inputFile=_path to file_**

add --showMetrics to see elapsed time and alert count

## Generate input file (Optional)
**npm run genData -- --numEntries=5000 --numSatellites=4**

**Warning**: Running genData will write to your filesystem at repo_location/data/input.generated.txt
after genData npm run main -- --inputFile=./data/input.generated.txt to process the new file

## Run linter
**npm run lint**

Uses 'standard' library - sometimes the spacing is wacky -- see main.js
 
# Assumptions
In addtion to assuming the input files are properly formatted, I also assumed that the events in the file are in order by timestamp. The example input file timestamps do not include a 'T' or a 'Z' that would indicate GMT - I assume that the input file is in GMT because the example output alerts do have ISO date strings.

# Approach
1. The sample input only triggers two alerts. The dataGenerator was written specifically to create test data the matches the input exactly - inclcuding date format ( lacking T and Z ). The data is randomized on satelliteID and raw value with the timestamp getting and increment between 100 and 500 ms. Since the alert interval is 300000ms, there will be many alerts recorded - which is useful for development.
2. The TelemetryProcessor includes the logic to create alerts based on the specified conditions. If the raw value is not in the RED range - low for battery, high for temp, the event is ignored. If the event is 'alertable' then the component type, satellite ID and timestamp millisecond value is recorded in the event tracker. The _eventTracker holds arrays for each satelliteId that have a maximum size of three based on the alerting requirements. Once the _eventTracker has three timestamps for a satelliteId, the first and last (index 0 and 2) timestamps are subtracted and the resulting millisecond value is compared against the alert interval. Anytime the tracked timestamps are within 5 minutes - the defined interval, an alert is created and stored in _alerts. Output is sent to the console when the end of file is reached
3. The TelemetryProcessor is independent of the input method. For this challenge, main.js is reading from the filesystem, but, the processor will accept a properly formatted event from any source e.g pub/sub (kafka), a database, a polled rest endpoint or a websocket.

# Improvements
- There are bable webpack plugins and configs that would allow the TelemetryProcessor fields and functions to be "private" as opposed to just commented in code as intended to be private. I wanted to avoid the added complexity of configuring webpack for this challenge
- For now there are only 2 component types of concern. The "severity" field is handled with a ternary in TelemetryProcessor._alert(). If additional component types were to be added, Alert may have to be it's own class
- To add an additional event type requires changes to TelemetryProcessor._eventValueOutsideRange(), _alert as mentioned earlier, and component ranges would have to be added to constants.js. Additionally, the number of events and the alert interval are all constants. Would suggest moving to external config file in a more prod like setting ( run in express ) so these values could be adjusted on the fly. This would also allow for per-component type configuration. It would be more flexible

- - -

# Paging Mission Control

> You are tasked with assisting satellite ground operations for an earth science mission that monitors magnetic field variations at the Earth's poles. A pair of satellites fly in tandem orbit such that at least one will have line of sight with a pole to take accurate readings. The satellite’s science instruments are sensitive to changes in temperature and must be monitored closely. Onboard thermostats take several temperature readings every minute to ensure that the precision magnetometers do not overheat. Battery systems voltage levels are also monitored to ensure that power is available to cooling coils. Design a monitoring and alert application that processes status telemetry from the satellites and generates alert messages in cases of certain limit violation scenarios.

## Requirements
Ingest status telemetry data and create alert messages for the following violation conditions:

- If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
- If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.

### Input Format
The program is to accept a file as input. The file is an ASCII text file containing pipe delimited records.

The ingest of status telemetry data has the format:

```
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
```

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify alert messages.  The alert messages should be valid JSON with the following properties:

```javascript
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
```

The program will output to screen or console (and not to a file). 

## Sample Data
The following may be used as sample input and output datasets.

### Input

```
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
```

### Ouput

```javascript
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
```




