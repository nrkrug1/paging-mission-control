'use strict'
const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const dateFormat = require('dateformat')
const args = require('yargs').argv
const constants = require('./constants')

// Create templates for Battery and Temp telemetry
// Telemetry entries are in the form of:
// [timestamp, sat id, red high, yellow high, yellow low, red low, value, type]
//
// Generator will insert timestamp, sat id and value
const batteryTelemetry = [
  null,
  null,
  constants.BATTERY_RED_HIGH,
  constants.BATTERY_YELLOW_HIGH,
  constants.BATTERY_YELLOW_LOW,
  constants.BATTERY_RED_LOW,
  null,
  constants.BATTERY
]

const tempTelemetry = [
  null,
  null,
  constants.TEMP_RED_HIGH,
  constants.TEMP_YELLOW_HIGH,
  constants.TEMP_YELLOW_LOW,
  constants.TEMP_RED_LOW,
  null,
  constants.TEMP
]

const dataGenerator = {}

/**
 * Generate file that matches the format of the challenge input example
 *
 * @return none
 */
dataGenerator.generateDataFile = function () {
  const dataStream = fs.createWriteStream(
    path.join(__dirname, constants.DATA_DIR, constants.GENERATED_FILE_NAME),
    { flags: 'a' })

  for (let i = 0; i < args.numEntries; i++) {
    const telemetry = _.join(this.generateTelemetry(satelliteIds), '|')
    dataStream.write(`${telemetry}\n`)
  }

  dataStream.end()
}

/**
 * Creates a numeric 1up id for the number of satellites specified
 *
 * @param {numSatellites} number of satellite ids to create
 * @return array of satellite ids
 */
dataGenerator.generateSatelliteIds = function (numSatellites) {
  const satelliteIds = []
  for (let i = 0; i < numSatellites; i++) {
    satelliteIds.push(constants.BASE_SATELLITE_ID + i)
  }
  return satelliteIds
}

/**
 * Returns a random satellite id using lodash's random funciton
 *
 * @param {satelliteIds} array of satelliteIds
 * @return random id
 */
dataGenerator.getRandomSatelliteId = function (satelliteIds) {
  if (_.isNil(satelliteIds)) return null

  return satelliteIds[_.random(0, satelliteIds.length - 1)]
}

/**
 * Generates a random raw value based on the component limits with an additional
 * range added for the 'Red' events
 *
 * @param {componentType} battery or temp
 * @return random appropriate raw value
 */
dataGenerator.generateRawValue = function (componentType) {
  if (_.isNil(componentType)) return null

  let val

  switch (componentType) {
    case constants.BATTERY:
      val = _.random(
        constants.BATTERY_RED_LOW - constants.BATTERY_RANGE,
        constants.BATTERY_RED_HIGH + constants.BATTERY_RANGE, true)
      break
    case constants.TEMP:
      val = _.random(
        constants.TEMP_RED_LOW - constants.TEMP_RANGE,
        constants.TEMP_RED_HIGH + constants.TEMP_RANGE, true)
      break
    default:
      val = null
      break
  }

  return _.isNil(val) ? null : _.round(val, 1)
}

/**
 * Generates an array of random telemetry values based on component type
 *
 * @param {satelliteIds} array of satelite ids
 * @return array of telemetry values
 */
dataGenerator.generateTelemetry = function (satelliteIds) {
  // Use random to decide between battery and temp event
  const telemetry =
    _.random(0, 1) === 1 ? [...batteryTelemetry] : [...tempTelemetry]

  // Set the date
  // Add milliseconds to prevent multiple entries with the same timestamp
  // also makes for more realistic test data
  telemetryStartDate
    .setMilliseconds(telemetryStartDate.getMilliseconds() + _.random(100, 500))
  telemetry[0] = dateFormat(telemetryStartDate, 'yyyymmdd HH:MM:ss.l')

  // Set the satellite id
  telemetry[1] = this.getRandomSatelliteId(satelliteIds)

  // Set the raw value
  telemetry[6] = this.generateRawValue(telemetry[7])

  return telemetry
}

module.exports = dataGenerator

const satelliteIds = dataGenerator.generateSatelliteIds(args.numSatellites)
const telemetryStartDate = new Date(_.random(2002, 2020), 0, 1)

// Main entry point if script is run from terminal
if (require.main === module) {
  console.log('Generating Telemetry Data...')
  const start = Date.now()
  dataGenerator.generateDataFile()
  const end = Date.now()
  console.log(`Complete! ${args.numEntries} entries written in ${end - start}ms`)
}
