'use-strict'
const _ = require('lodash')

const util = {}

util.parseDate = function (dateString) {
  const dateObj = new Date()
  const dateParts = _.split(dateString, ' ')
  dateObj.setYear(dateParts[0].substring(0, 4))
  dateObj.setMonth(dateParts[0].substring(4, 6) - 1)
  dateObj.setDate(dateParts[0].substring(6, 8))

  const timeParts = _.split(dateParts[1], ':')
  dateObj.setHours(timeParts[0])
  dateObj.setMinutes(timeParts[1])

  const secondsPart = _.split(timeParts[2], '.')
  dateObj.setSeconds(secondsPart[0])
  dateObj.setMilliseconds(secondsPart[1])

  return dateObj
}

module.exports = util
