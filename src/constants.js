'use strict'
module.exports = Object.freeze({
  // Output file fileName
  DATA_DIR: '../data',
  FILE_NAME: 'input.txt',
  GENERATED_FILE_NAME: 'input.generated.txt',

  // Telemetry Types
  BATTERY: 'BATT',
  TEMP: 'TSTAT',

  // Telemetry limits
  BATTERY_RED_HIGH: 17,
  BATTERY_YELLOW_HIGH: 15,
  BATTERY_YELLOW_LOW: 9,
  BATTERY_RED_LOW: 8,
  TEMP_RED_HIGH: 101,
  TEMP_YELLOW_HIGH: 98,
  TEMP_YELLOW_LOW: 25,
  TEMP_RED_LOW: 20,

  BATTERY_RANGE: 7,
  TEMP_RANGE: 19,

  // satellite vals
  BASE_SATELLITE_ID: 1000,

  // 5 minutes in milliseconds
  ALERT_INTERVAL_MS: 300000
})
