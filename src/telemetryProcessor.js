'use strict'
const _ = require('lodash')

const constants = require('./constants')
const util = require('./util')

class TelemetryProcessor {
  constructor () {
    // Intended to be private - needs babel/webpack
    this._eventTracker = {}
    this._alerts = []
  }

  /**
   * Compares event raw value to defined limits. Records event if value outside
   * range. Creates an alert if 3 events are recorded within the alert interval
   *
   * @param {event} array of 8 elements of the expected format.
   * @return {boolean} true if alert was created during processing
   */
  processEvent (event) {
    if (_.isNil(event) || _.size(event !== 8) || !_.isArray(event)) return

    let alertCreated = false

    // Ignore events with acceptable values
    if (this._eventValueOutsideRange(event)) {
      this._recordEvent(event)
      alertCreated = this._alert(event)
    }

    return alertCreated
  }

  getAlerts () {
    return this._alerts
  }

  clear () {
    this._eventTracker = {}
    this._alerts = []
  }

  /**
   * Checks event raw value against the limit defined by reqs. Different for
   * each component type.
   * Note: Intended to be private - needs babel/webpack
   *
   * @param {event} array of 8 elements of the expected format.
   * @return {boolean} true if event raw value is outside defined range.
   */
  _eventValueOutsideRange (event) {
    const value = event[6]
    const componentType = event[7]
    let outsideRange = false

    switch (componentType) {
      case constants.BATTERY:
        outsideRange = value < constants.BATTERY_RED_LOW
        break
      case constants.TEMP:
        outsideRange = value > constants.TEMP_RED_HIGH
        break
      default:
        console.log('Error: Unupported component type')
        outsideRange = false
    }
    return outsideRange
  }

  /**
   * Saves an event timestamp by component type and satellite id. Maintains of
   * maximum size of 3. Modifies _eventTracker.
   * Note: Intended to be private - needs babel/webpack
   *
   * @param {event} array of 8 elements of the expected format.
   * @return none
   */
  _recordEvent (event) {
    const satelliteId = event[1]
    const componentType = event[7]

    // If first event, create the tracker structure
    if (_.isNil(this._eventTracker[componentType])) {
      this._eventTracker[componentType] = {}
    }
    const componentTracker = this._eventTracker[componentType]

    if (_.isNil(componentTracker[satelliteId])) {
      componentTracker[satelliteId] = []
    }

    // Record value
    const eventTimestamp = util.parseDate(event[0])
    componentTracker[satelliteId].push(eventTimestamp.valueOf())

    // Maintain size of 3 based one alerting reqs
    if (_.size(componentTracker[satelliteId]) === 4) {
      // shift off the oldest
      componentTracker[satelliteId].shift()
    }
  }

  /**
   * Creates an alert if 3 events are in the applicable component/satellite
   * event tracker and the three events occured within the alterinterval
   * Modifies _alerts
   * Note: Intended to be private - needs babel/webpack
   *
   * @param {event} array of 8 elements of the expected format.
   * @return {boolean} true if alert created, false otherwise.
   */
  _alert (event) {
    const satelliteId = event[1]
    const componentType = event[7]

    const eventTracker =
      _.get(this._eventTracker, `${componentType}.${satelliteId}`, [])

    // Per reqs - there has to be 3 events to alert
    if (_.size(eventTracker) === 3) {
      const firstEventTime = eventTracker[0]
      const lastEventTime = eventTracker[2]

      // The 3 events have to be within the alert interval
      if ((lastEventTime - firstEventTime) < constants.ALERT_INTERVAL_MS) {
        // Deal with the date
        // Output requirements appear to be GMT date.
        // Input does not specify timezone - Assuming GMT
        const eventDate = new Date(firstEventTime)
        const isoEventDate =
          new Date(eventDate.getTime() - eventDate.getTimezoneOffset() * 60000)

        // Alert timestamp is set to the firstEventTime within the alert interval
        const alert = {
          satelliteId: parseInt(event[1]),
          severity: `RED ${event[7] === constants.BATTERY ? 'LOW' : 'HIGH'}`,
          component: event[7],
          timestamp: isoEventDate.toISOString()
        }

        this._alerts.push(alert)

        // Return indication that alert was created
        return true
      }
    }
  }
}

module.exports.TelemetryProcessor = TelemetryProcessor
