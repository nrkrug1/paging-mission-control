'use strict'
const path = require('path')
const readline = require('linebyline')
const _ = require('lodash')
const args = require('yargs').argv
const prettier = require('prettier')

const { TelemetryProcessor } = require('./telemetryProcessor')
const constants = require('./constants')

function main () {
  const telemetryProcessor = new TelemetryProcessor()

  let start
  if (args.showMetrics === true) {
    console.log('Processing Telemetry Data...')
    start = Date.now()
  }

  // Accept input file path as cmd arg
  const inputFile = args.inputFile ||
    path.join(__dirname, constants.DATA_DIR, constants.FILE_NAME)

  const rl = readline(inputFile)
  rl.on('line', function (line) {
    // Create and array from input and process
    const event = _.split(line.toString(), '|')
    telemetryProcessor.processEvent(event)
  })
    .on('error', function (e) {
      console.log(e)
    })
    .on('end', function () {
      const alerts = telemetryProcessor.getAlerts()

      // Output JSON per reqs
      console.log(prettier.format(JSON.stringify(alerts), { semi: false, parser: 'json' }))

      if (args.showMetrics === true) {
        const end = Date.now()
        const processTime = end - start
        const alertCount = _.size(alerts)
        console.log(`Processing complete. ${alertCount} alerts in ${processTime}ms`)
      }
    })
}

// Main entry point if script is run from terminal
if (require.main === module) {
  main()
}
